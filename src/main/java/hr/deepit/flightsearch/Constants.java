package hr.deepit.flightsearch;

public class Constants {
    public final static String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
    public final static String DATE_FORMAT = "yyyy-MM-dd";

}
