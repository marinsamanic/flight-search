package hr.deepit.flightsearch.bvo;

import java.time.LocalDateTime;

public class OneDirectionFlightOfferBVO {
    private String originIATA;
    private String destinationIATA;

    private String originFullName;
    private String destinationFullName;

    private LocalDateTime departureLocalDateTime;
    private LocalDateTime arrivalLocalDateTime;

    private Integer stops;

    public String getOriginIATA() {
        return originIATA;
    }

    public void setOriginIATA(String originIATA) {
        this.originIATA = originIATA;
    }

    public String getDestinationIATA() {
        return destinationIATA;
    }

    public void setDestinationIATA(String destinationIATA) {
        this.destinationIATA = destinationIATA;
    }

    public Integer getStops() {
        return stops;
    }

    public String getStopsPretty() {
        return stops == 0 ? "Direct" : stops == 1 ? stops.toString() + " stop" : stops.toString() + " stops";
    }

    public void setStops(Integer stops) {
        this.stops = stops;
    }

    public LocalDateTime getDepartureLocalDateTime() {
        return departureLocalDateTime;
    }

    public void setDepartureLocalDateTime(LocalDateTime departureLocalDateTime) {
        this.departureLocalDateTime = departureLocalDateTime;
    }

    public LocalDateTime getArrivalLocalDateTime() {
        return arrivalLocalDateTime;
    }

    public void setArrivalLocalDateTime(LocalDateTime arrivalLocalDateTime) {
        this.arrivalLocalDateTime = arrivalLocalDateTime;
    }

    public String getOriginFullName() {
        return originFullName;
    }

    public void setOriginFullName(String originFullName) {
        this.originFullName = originFullName;
    }

    public String getDestinationFullName() {
        return destinationFullName;
    }

    public void setDestinationFullName(String destinationFullName) {
        this.destinationFullName = destinationFullName;
    }

    @Override
    public String
    toString() {
        return "OneDirectionFlightOfferBVO{" +
                "originIATA='" + originIATA + '\'' +
                ", destinationIATA='" + destinationIATA + '\'' +
                ", originFullName='" + originFullName + '\'' +
                ", destinationFullName='" + destinationFullName + '\'' +
                ", departureLocalDateTime=" + departureLocalDateTime +
                ", arrivalLocalDateTime=" + arrivalLocalDateTime +
                ", stops=" + stops +
                '}';
    }
}
