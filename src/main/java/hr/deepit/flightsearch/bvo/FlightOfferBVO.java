package hr.deepit.flightsearch.bvo;

public class FlightOfferBVO {
    private Integer numberOfStops;
    private Integer numberOfTravelers;
    private String currency;
    private Double totalPrice;

    private OneDirectionFlightOfferBVO outboundFlight;
    private OneDirectionFlightOfferBVO returnFlight;

    public Integer getNumberOfStops() {
        return numberOfStops;
    }

    public void setNumberOfStops(Integer numberOfStops) {
        this.numberOfStops = numberOfStops;
    }

    public Integer getNumberOfTravelers() {
        return numberOfTravelers;
    }

    public void setNumberOfTravelers(Integer numberOfTravelers) {
        this.numberOfTravelers = numberOfTravelers;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OneDirectionFlightOfferBVO getOutboundFlight() {
        return outboundFlight;
    }

    public void setOutboundFlight(OneDirectionFlightOfferBVO outboundFlight) {
        this.outboundFlight = outboundFlight;
    }

    public OneDirectionFlightOfferBVO getReturnFlight() {
        return returnFlight;
    }

    public void setReturnFlight(OneDirectionFlightOfferBVO returnFlight) {
        this.returnFlight = returnFlight;
    }

    public String getNumberOfStopsPretty() {
        return numberOfStops == 0 ? "Direct" : numberOfStops == 1 ? numberOfStops.toString() + " stop" : numberOfStops.toString() + " stops";
    }

    @Override
    public String toString() {
        return "FlightOfferBVO{" +
                "numberOfStops=" + numberOfStops +
                ", numberOfTravelers=" + numberOfTravelers +
                ", currency='" + currency + '\'' +
                ", totalPrice=" + totalPrice +
                ", outboundFlight=" + outboundFlight +
                ", returnFlight=" + returnFlight +
                '}';
    }
}
