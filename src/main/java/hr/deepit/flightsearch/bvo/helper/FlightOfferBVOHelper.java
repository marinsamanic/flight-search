package hr.deepit.flightsearch.bvo.helper;

import com.amadeus.Response;
import com.amadeus.resources.FlightOffer;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import hr.deepit.flightsearch.Constants;
import hr.deepit.flightsearch.bvo.FlightOfferBVO;
import hr.deepit.flightsearch.bvo.OneDirectionFlightOfferBVO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlightOfferBVOHelper {

    public static List<FlightOfferBVO> createListOfFlightOfferBVOs(FlightOffer[] flightOffers, Integer numberOfTravelers) {
        List<FlightOfferBVO> flightOfferBVOS = new ArrayList<>();
        Map<String, LinkedTreeMap<String, String>> namesByIataCodeMap = fillIataNames(flightOffers[0].getResponse());

        String currency = flightOffers[0].getResponse().getResult().get("meta").
                getAsJsonObject().get("currency").getAsString();

        for (FlightOffer flightOffer : flightOffers) {
            //postoji više ponuda za isti let (ECONOMY, BUSINESS klasa)
            for (FlightOffer.OfferItem offerItem : flightOffer.getOfferItems()) {
                flightOfferBVOS.add(createFlightOfferBVO(offerItem, currency, numberOfTravelers, namesByIataCodeMap));
            }
        }
        return flightOfferBVOS;
    }

    private static FlightOfferBVO createFlightOfferBVO(FlightOffer.OfferItem offerItem, String currency,
                                                       Integer numberOfTravelers, Map<String, LinkedTreeMap<String, String>> namesByIataCodeMap) {
        FlightOfferBVO flightOfferBVO = new FlightOfferBVO();
        flightOfferBVO.setCurrency(currency);
        flightOfferBVO.setNumberOfTravelers(numberOfTravelers != null ? numberOfTravelers : 1);

        Double totalPrice = offerItem.getPrice().getTotal() + offerItem.getPrice().getTotalTaxes();
        BigDecimal bd = new BigDecimal(totalPrice);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        totalPrice = bd.doubleValue();

        flightOfferBVO.setTotalPrice(totalPrice);

        FlightOffer.Service outboundFlightService = offerItem.getServices()[0];
        FlightOffer.Service returnFlightService = null;

        if (offerItem.getServices().length > 1) {
            returnFlightService = offerItem.getServices()[1];
        }

        OneDirectionFlightOfferBVO outboundFlightOffer = createOneDirectionFlightOfferBVO(outboundFlightService, namesByIataCodeMap);
        OneDirectionFlightOfferBVO returnFlightOffer = returnFlightService == null ? null : createOneDirectionFlightOfferBVO(returnFlightService, namesByIataCodeMap);

        flightOfferBVO.setNumberOfStops(outboundFlightOffer.getStops());
        flightOfferBVO.setOutboundFlight(outboundFlightOffer);
        flightOfferBVO.setReturnFlight(returnFlightOffer);
        return flightOfferBVO;
    }

    private static OneDirectionFlightOfferBVO createOneDirectionFlightOfferBVO(FlightOffer.Service service, Map<String, 
    		LinkedTreeMap<String, String>> namesByIataCodeMap) {
        OneDirectionFlightOfferBVO oneDirectionFlightOfferBVO = new OneDirectionFlightOfferBVO();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT);

        int firstSegmentIndex = 0;
        int lastSegmentindex = service.getSegments().length - 1;

        String departureDateAndTimeString = service.getSegments()[firstSegmentIndex].getFlightSegment().getDeparture().getAt();
        oneDirectionFlightOfferBVO.setDepartureLocalDateTime(LocalDateTime.parse(departureDateAndTimeString, formatter));
        oneDirectionFlightOfferBVO.setOriginIATA(service.getSegments()[firstSegmentIndex].getFlightSegment().getDeparture().getIataCode());

        String arrivalDateAndTimeString = service.getSegments()[lastSegmentindex].getFlightSegment().getArrival().getAt();
        oneDirectionFlightOfferBVO.setArrivalLocalDateTime(LocalDateTime.parse(arrivalDateAndTimeString, formatter));
        oneDirectionFlightOfferBVO.setDestinationIATA(service.getSegments()[lastSegmentindex].getFlightSegment().getArrival().getIataCode());

        oneDirectionFlightOfferBVO.setStops(service.getSegments().length - 1);
        oneDirectionFlightOfferBVO.setOriginFullName(namesByIataCodeMap.get(oneDirectionFlightOfferBVO.getOriginIATA()).get("detailedName").toString());
        oneDirectionFlightOfferBVO.setDestinationFullName(namesByIataCodeMap.get(oneDirectionFlightOfferBVO.getDestinationIATA()).get("detailedName").toString());

        return oneDirectionFlightOfferBVO;
    }

    private static Map<String, LinkedTreeMap<String, String>> fillIataNames(Response response) {
        JsonObject locationJsonObject = response.getResult().get("dictionaries").getAsJsonObject().get("locations").getAsJsonObject();
        HashMap<String, LinkedTreeMap<String, String>> locationsHashMap;
        locationsHashMap = new Gson().fromJson(locationJsonObject.toString(), HashMap.class);
        return locationsHashMap;
    }
}
