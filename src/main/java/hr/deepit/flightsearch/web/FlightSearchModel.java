package hr.deepit.flightsearch.web;

import hr.deepit.flightsearch.bvo.FlightOfferBVO;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Named
public class FlightSearchModel {
    private String originIATA;
    private String destinationIATA;
    private Date departureDate;
    private Date returnDate;
    private Integer numberOfTravelers;
    private String currency;

    List<FlightOfferBVO> flightOfferBVOS = new ArrayList<>();

    public void reset() {
        originIATA = null;
        destinationIATA = null;
        departureDate = null;
        returnDate = null;
        numberOfTravelers = null;
        currency = null;
        flightOfferBVOS = new ArrayList<>();
    }

    public void resetResults() {
        flightOfferBVOS = new ArrayList<>();
    }

    public String getOriginIATA() {
        return originIATA;
    }

    public void setOriginIATA(String originIATA) {
        this.originIATA = originIATA;
    }

    public String getDestinationIATA() {
        return destinationIATA;
    }

    public void setDestinationIATA(String destinationIATA) {
        this.destinationIATA = destinationIATA;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Integer getNumberOfTravelers() {
        return numberOfTravelers;
    }

    public void setNumberOfTravelers(Integer numberOfTravelers) {
        this.numberOfTravelers = numberOfTravelers;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<FlightOfferBVO> getFlightOfferBVOS() {
        return flightOfferBVOS;
    }

    public void setFlightOfferBVOS(List<FlightOfferBVO> flightOfferBVOS) {
        this.flightOfferBVOS = flightOfferBVOS;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Date getReturnMinDate() {
        return departureDate == null ? new Date() : departureDate;
    }
}
