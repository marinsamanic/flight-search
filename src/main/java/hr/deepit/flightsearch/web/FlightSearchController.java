package hr.deepit.flightsearch.web;

import com.amadeus.exceptions.ClientException;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOffer;
import hr.deepit.flightsearch.bvo.FlightOfferBVO;
import hr.deepit.flightsearch.bvo.helper.FlightOfferBVOHelper;
import hr.deepit.flightsearch.service.AmadeusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class FlightSearchController {
    Logger logger = LoggerFactory.getLogger(FlightSearchController.class);

    @Inject
    private FlightSearchModel model;

    @Inject
    private AmadeusService amadeusService;

    @Inject
    private MessageProvider provider;

    @Inject
    private FacesContext facesContext;

    public void loadFlights() {
        List<FlightOfferBVO> flightOfferBVOS;
        try {
            if (validate()) {
                FlightOffer[] flightOffers = amadeusService.searchFlightOffers(model.getOriginIATA(), model.getDestinationIATA(),
                        model.getDepartureDate(), model.getReturnDate(), model.getNumberOfTravelers(), model.getCurrency());

                if (flightOffers[0].getResponse().getStatusCode() != 200) {
                    model.resetResults();
                    logger.error("Wrong status code for Flight Low-fare Search: " + flightOffers[0].getResponse().getStatusCode());
                    facesContext.addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            provider.getValue("system_error"), null));
                } else {
                    flightOfferBVOS = FlightOfferBVOHelper.createListOfFlightOfferBVOs(flightOffers, model.getNumberOfTravelers());
                    model.setFlightOfferBVOS(flightOfferBVOS);
                }
            }
        } catch (ClientException e) {
            model.resetResults();
            logger.warn("Amadeus responded with ClientEcxeption: " + e.getMessage());
            String[] lines = e.getDescription().split("\\r?\\n");

            for (String line : lines) {
                facesContext.addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        line, null));
            }
        } catch (ResponseException | RuntimeException e) {
            model.resetResults();
            logger.error(e.getMessage(), e);
            facesContext.addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    provider.getValue("system_error"), null));
        }
    }

    private boolean validate() {
        boolean valid = true;
        if (model.getReturnDate() != null && model.getReturnDate().before(model.getDepartureDate())) {
            facesContext.addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    provider.getValue("flight.search.validation.message.return_date_before_departure"), null));
            valid = false;
        }
        return valid;
    }
}
