package hr.deepit.flightsearch.service;

import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOffer;
import hr.deepit.flightsearch.Constants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class AmadeusServiceImpl implements AmadeusService {
    Logger logger = LoggerFactory.getLogger(AmadeusServiceImpl.class);

    private static Amadeus amadeus = Amadeus
            .builder("asaQMRCNGuG66LAfi2Meue2f9chKflF2", "gJpivokJNa4OoAhu")
            .build();

    @Override
    @Cacheable("flightOffers")
    public FlightOffer[] searchFlightOffers(String originIATA, String destintionIATA, Date departureDate,
                                            Date returnDate, Integer numberOfTravelers, String currency) throws ResponseException {

        logger.debug("Searching flight offers");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
        String departureDateString = simpleDateFormat.format(departureDate);

        Params params = Params.
                with("origin", originIATA).
                and("destination", destintionIATA).
                and("departureDate", departureDateString);

        if (returnDate != null) {
            String returnDateString = simpleDateFormat.format(returnDate);
            params.and("returnDate", returnDateString);
        }

        if (numberOfTravelers != null) {
            params.and("adults", numberOfTravelers);
        }

        if (StringUtils.isNotBlank(currency)) {
            params.and("currency", currency);
        }

        return amadeus.shopping.flightOffers.get(params);
    }
}
