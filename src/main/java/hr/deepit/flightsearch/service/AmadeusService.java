package hr.deepit.flightsearch.service;

import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOffer;

import java.util.Date;

public interface AmadeusService {

    FlightOffer[] searchFlightOffers(String originIATA, String destintionIATA, Date departureDate,
                                     Date returnDate, Integer numberOfTravelers, String currency) throws ResponseException;
}
