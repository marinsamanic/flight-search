package hr.deepit.flightsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SpringPrimeFacesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringPrimeFacesApplication.class, args);
    }
}
